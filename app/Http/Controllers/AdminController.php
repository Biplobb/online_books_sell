<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Session;

class AdminController extends Controller
{
   public function index(){
       return view('admin.admin_login');
   }
    public function registration(){


       return view('admin.registration.create');
    }
    public function registrationstore(Request $request){
        $data = array();
        $data['name'] = $request->name;
        $data['username'] = $request->username;
        $data['phone'] = $request->phone;
        $data['created_at'] =date("Y-m-d H:i:s A");

        $data['password'] =$request->password;


        $image = $request->file('image');

        if ($image) {

            $ext=$image->getClientOriginalName();
            $image_full_name = $ext.time();
            $upload_path = 'issue_image/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
                $data['image'] = $image_url;
                DB::table('tbl_admin_login')->insert($data);
                Session::put('message', 'Register   Successfully !');

                return view('admin.admin_login');
            }


        }




        return view('admin.admin_login');
    }

   public function AdminIndex(){
       $id = Session::get('id');
       if ($id == NULL) {
           return Redirect::to('/admin-panel')->send();
       }
       return view('admin.index');
   }
   public function auth_check(Request $request){

       $username = $request->username;
       $password = $request->password;

        $result = DB::table('tbl_admin_login')
           ->where('username', $username)
           ->where('password', $password)
           ->first();
   /* $resultshow = DB::table('tbl_admin_login') ->select('phone')->where('username', $username)->get();*/
       if($result){
           Session::put('id',$result->id);
           return Redirect::to('/admin');
       }else{
           Session::put('exception','Email or Password Invalid');
           return Redirect::to('admin-panel');
       }

   }


    public function logout() {
        Session::put('id', null);
        Session::put('message', 'You are successfully logout!!');
        return Redirect::to('/admin-panel');
    }

}
