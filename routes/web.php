<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.welcome');
});
Route::get('/admin','admincontroller@index');
Route::get('/admin-registration/','admincontroller@registration');
Route::post('//registration/store/','admincontroller@registrationstore');
Route::get('/admin','AdminController@AdminIndex');

Route::get('/logout','AdminController@logout');

Route::get('/admin-panel','AdminController@index');

Route::post('/admin-login-check','AdminController@auth_check');



//book sell
Route::get('/add-book','Booksell@create');
Route::post('/booksell/store','Booksell@store');