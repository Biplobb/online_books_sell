<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
            <a href="{{URL::to('/admin')}}">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/add-book/')}}">
                <i class="material-icons">text_fields</i>
                <span>Add book</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/manage-book')}}">
                <i class="material-icons">view_list</i>
                <span>All book</span>
            </a>
        </li>




    </ul>
</div>