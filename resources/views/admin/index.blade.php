@include('admin.layouts.head')
<body class="theme-red">


<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="#">Books - ADMIN PANEL</a>
        </div>

        <div style="float: right;margin-top: 15px;">
            <a class="btn btn-success" href="{{URL::to('/logout')}}">Logout</a>
        </div>

    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">


     @include('admin.layouts.menu')

    </aside>

</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>

    @yield('main-content')

        @show


    </div>
</section>

@include('admin.layouts.footer')